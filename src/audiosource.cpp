#include <acaudio/audiosource.h>

namespace ac {

AudioSource::AudioSource(const AudioSource &copy)
{
    alCheck(alGenSources(1, &m_source));
    alCheck(alSourcei(m_source, AL_BUFFER, 0));

    setVolume(copy.volume());
}

AudioSource::~AudioSource()
{
    alCheck(alSourcei(m_source, AL_BUFFER, 0));
    alCheck(alDeleteSources(1, &m_source));
}

void AudioSource::setVolume(float volume)
{
    alCheck(alSourcef(m_source, AL_GAIN, volume * 0.01f));
}

float AudioSource::volume() const
{
    ALfloat gain;
    alCheck(alGetSourcef(m_source, AL_GAIN, &gain));
    return gain * 100.f;
}

AudioSource &AudioSource::operator =(const AudioSource &right)
{
    setVolume(right.volume());
    return *this;
}

AudioSource::AudioSource()
{
    alCheck(alGenSources(1, &m_source));
    alCheck(alSourcei(m_source, AL_BUFFER, 0));
}

AudioSource::Status AudioSource::status() const
{
    ALint status;
    alCheck(alGetSourcei(m_source, AL_SOURCE_STATE, &status));
    switch (status) {
        case AL_INITIAL:
        case AL_STOPPED: return Stopped;
        case AL_PAUSED:  return Paused;
        case AL_PLAYING: return Playing;
    }
    return Stopped;
}

}
