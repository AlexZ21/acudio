#include <acaudio/audiotrack.h>

namespace ac {

AudioTrack::AudioTrack(const std::string &source, const std::string &loader) :
    m_source(source),
    m_loader(loader)
{

}

std::string AudioTrack::source() const
{
    return m_source;
}

void AudioTrack::setSource(const std::string &source)
{
    m_source = source;
}

std::string AudioTrack::loader() const
{
    return m_loader;
}

void AudioTrack::setLoader(const std::string &loader)
{
    m_loader = loader;
}

}
