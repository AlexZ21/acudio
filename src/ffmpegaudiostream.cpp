#include <acaudio/ffmpegaudiostream.h>

#include <iostream>

namespace ac {

FFMpegAudioStream::FFMpegAudioStream() :
    AudioStream(),
    m_audioTrackUpdated(false)
{

}

FFMpegAudioStream::~FFMpegAudioStream()
{
    stop();
    m_ffmpegContext.reset();
    m_currentAudioLoader.reset();
}

AudioTrack FFMpegAudioStream::audioTrack() const
{
    return m_curentAudioTrack;
}

void FFMpegAudioStream::setAudioTrack(const AudioTrack &audioTrack)
{
    m_curentAudioTrack = audioTrack;
    m_audioTrackUpdated = true;
}

void FFMpegAudioStream::play()
{
    if (m_audioTrackUpdated) {
        m_audioTrackUpdated = false;

        if (status() == Playing)
            stop();

        std::cout << "Playing: " << m_curentAudioTrack.source() << std::endl;
        if (m_curentAudioTrack.source().empty())
            return;


        m_currentAudioLoader.reset(AudioLoaderFactory::create(m_curentAudioTrack.loader()));
        if (!m_currentAudioLoader.get())
            return;

        m_ffmpegContext.reset(new FFMpegWrap(m_currentAudioLoader.get(), 44100, 2));
        if (!m_ffmpegContext.get()) {
            m_currentAudioLoader.reset();
            return;
        }

        m_currentAudioLoader->load(m_curentAudioTrack.source());

        if (!m_ffmpegContext->init()) {
            m_currentAudioLoader.reset();
            m_ffmpegContext.reset();
            return;
        }

        m_outputSamplesBuffer.clear();
        m_outputSamplesBuffer.resize(m_ffmpegContext->outputChanels() * m_ffmpegContext->outputSampleRate() * 2);

        initialize(m_ffmpegContext->outputChanels(), m_ffmpegContext->outputSampleRate());
    }

    if (!m_currentAudioLoader.get() && !m_ffmpegContext.get())
        return;

    AudioStream::play();
}

bool FFMpegAudioStream::onData(AudioStream::Chunk &data)
{
    if (!m_ffmpegContext.get()) {
        data.samples = 0;
        data.sampleCount = 0;
        return false;
    }

    data.samples = m_outputSamplesBuffer.data();
    data.sampleCount = 0;

    while (data.sampleCount < m_ffmpegContext->outputChanels() * m_ffmpegContext->outputSampleRate()) {
        std::vector<uint8_t> samples;
        int nbSamples = 0;
        m_ffmpegContext->readSamples(samples, nbSamples);
        if (nbSamples < 0)
            break;
        memcpy((void*)(data.samples + data.sampleCount), samples.data(), samples.size());
        data.sampleCount += nbSamples;
    }

    return true;
}

void FFMpegAudioStream::onSeek(const std::chrono::milliseconds &timeOffset)
{
    if (!m_ffmpegContext.get())
        return;
    m_ffmpegContext->seek(timeOffset);
}

bool FFMpegAudioStream::createAudioLoader()
{
    if (m_curentAudioTrack.loader().empty())
        return false;
    AudioLoader *loader = AudioLoaderFactory::create(m_curentAudioTrack.loader());
    if (!loader)
        return false;
    m_currentAudioLoader.reset(loader);
    return true;
}

}
