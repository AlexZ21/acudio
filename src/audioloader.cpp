#include <acaudio/audioloader.h>

#include <fstream>
#include <cstring>
#include <iostream>

namespace ac {

AudioLoader::AudioLoader()
{

}

FileAudioLoader::FileAudioLoader()
{

}

void FileAudioLoader::load(const std::string &url)
{
    std::ifstream ifs(url);

    if (!ifs.eof() && !ifs.fail()) {
        ifs.seekg(0, std::ios_base::end);
        std::streampos fileSize = ifs.tellg();
        m_buffer.resize(fileSize);
        ifs.seekg(0, std::ios_base::beg);
        ifs.read(&m_buffer[0], fileSize);
    }

    std::cout << "Loaded bytes: " << m_buffer.size() << std::endl;
}

void FileAudioLoader::stop()
{

}

int64_t FileAudioLoader::read(uint8_t *data, int size, int pos)
{
    const char *data_ = m_buffer.data();
    int size_ = size;
    if (size + pos > m_buffer.size())
        size_ = m_buffer.size() - pos;
    if (size_ < 0)
        size_ = 0;
    std::memcpy(data, &data_[pos], size_);
    return size_;
}

int64_t FileAudioLoader::seek(int64_t pos, int64_t offset, int whence)
{
    return 0;
}

}
