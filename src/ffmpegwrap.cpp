#include <acaudio/ffmpegwrap.h>
#include <acaudio/audioloader.h>

#include <assert.h>
#include <iostream>
#include <cstring>
#include <algorithm>

#define CHECK(_expr_, _text_) \
    if (_expr_) { \
    std::cerr << _text_; \
    freeAll(); \
    return false; \
    }

namespace ac {

FFMpegWrap::FFMpegWrap(AudioLoader *audioLoader, int32_t outputSampleRate, int32_t outputChanels) :
    m_audioLoader(audioLoader),
    m_outputSampleRate(outputSampleRate),
    m_outputChanels(outputChanels),
    m_pos(0),
    m_avioContext(nullptr),
    m_audioContext(nullptr),
    m_streamId(-1),
    m_codec(nullptr),
    m_codecContext(nullptr),
    m_swrContext(nullptr),
    m_outputSamples(1024),
    m_maxOutputSamples(1024),
    m_outputData(nullptr)
{
    assert(audioLoader && "Audio loader is null");
    av_register_all();
}

FFMpegWrap::~FFMpegWrap()
{
    freeAll();
}

bool FFMpegWrap::init()
{
    m_avioContextBuffer.resize(1024);
    m_avioContext = avio_alloc_context(m_avioContextBuffer.data(),
                                       m_avioContextBuffer.size(),
                                       0,
                                       (void *)this,
                                       &FFMpegWrap::readBuffer,
                                       0,
                                       &FFMpegWrap::seekBuffer);
    CHECK(!m_avioContext, "FFMpegWrap: error allocate AVIOContext");

    m_audioContext = avformat_alloc_context();
    CHECK(!m_audioContext, "FFMpegWrap: error allocate AVFormatContext");

    m_audioContext->pb = m_avioContext;

    CHECK(avformat_open_input(&m_audioContext, 0, 0, 0) < 0,
          "FFMpegAudioStream: could not open input context");

    CHECK(avformat_find_stream_info(m_audioContext, nullptr) < 0,
          "FFMpegAudioStream: could not find input info");

    m_streamId = av_find_best_stream(m_audioContext, AVMEDIA_TYPE_AUDIO, -1, -1, &m_codec, 0);
    CHECK(m_streamId < 0, "FFMpegAudioStream: could not find stream");

    m_codecContext = avcodec_alloc_context3(nullptr);
    CHECK(!m_codecContext, "FFMpegWrap: error allocate AVCodecContext");

    CHECK(avcodec_parameters_to_context(m_codecContext, m_audioContext->streams[m_streamId]->codecpar) < 0,
          "FFMpegWrap: unable to avcodec_parameters_to_context");

    av_codec_set_pkt_timebase(m_codecContext, m_audioContext->streams[m_streamId]->time_base);
    av_opt_set_int(m_codecContext, "refcounted_frames", 1, 0);

    CHECK(avcodec_open2(m_codecContext, m_codec, 0) < 0, "FFMpegWrap: could not open codec");

    m_swrContext = swr_alloc();
    CHECK(!m_swrContext, "FFMpegWrap: error allocate SwrContext");

    AVCodecParameters *codecParams = m_audioContext->streams[m_streamId]->codecpar;

    uint64_t inLayout = codecParams->channel_layout;
    if (!inLayout) {
        int channelsCount = codecParams->channels;
        switch (channelsCount) {
        case 1: inLayout = AV_CH_LAYOUT_MONO; break;
        case 2: inLayout = AV_CH_LAYOUT_STEREO; break;
        default: std::cerr << "FFMpegWrap: unknown channel layout"; break;
        }
    }

    AVSampleFormat inFormat = m_codecContext->sample_fmt;

    m_outputSampleRate = 44100;
    m_outputChanels = codecParams->channels;

    av_opt_set_int(m_swrContext, "in_channel_layout", inLayout, 0);
    av_opt_set_int(m_swrContext, "in_sample_rate", codecParams->sample_rate, 0);
    av_opt_set_sample_fmt(m_swrContext, "in_sample_fmt", inFormat, 0);
    av_opt_set_int(m_swrContext, "out_channel_layout", inLayout, 0);
    av_opt_set_int(m_swrContext, "out_sample_rate", m_outputSampleRate, 0);
    av_opt_set_sample_fmt(m_swrContext, "out_sample_fmt", AV_SAMPLE_FMT_S16, 0);

    CHECK(swr_init(m_swrContext) < 0, "FFMpegWrap: could not init SwrContext");

    CHECK(av_samples_alloc_array_and_samples(&m_outputData, nullptr,
                                             m_outputChanels, m_outputSamples,
                                             AV_SAMPLE_FMT_S16, 0) < 0,
          "FFMpegWrap: could not allocate samples");


    m_audioContext->event_flags = AVFMT_EVENT_FLAG_METADATA_UPDATED;

    std::cout << "Detected codec: " << m_codec->name << std::endl;

    std::vector<std::string> codecNameFix = { "vorbis" };
    if (std::find(codecNameFix.begin(), codecNameFix.end(), m_codec->name) != codecNameFix.end())
        m_pos = 0;

    return true;
}

int32_t FFMpegWrap::outputSampleRate() const
{
    return m_outputSampleRate;
}

int32_t FFMpegWrap::outputChanels() const
{
    return m_outputChanels;
}

void FFMpegWrap::readSamples(std::vector<uint8_t> &samples, int &nbSamples)
{
    AVPacket pkt = {0};
    av_init_packet(&pkt);

    if (av_read_frame(m_audioContext, &pkt) < 0) {
        av_packet_unref(&pkt);
        return;
    }

    std::vector<AVFrame *> frames;
    decodePacket(&pkt, frames);

    if (frames.size() > 0) {
        for (AVFrame *frm : frames) {
            if (!frm)
                continue;

            uint8_t *samples_ = nullptr;
            int nbSamples_ = 0;
            int samplesLength_ = 0;

            resampleFrame(frm, samples_, nbSamples_, samplesLength_);

            samples.resize(samples.size() + samplesLength_);

            std::memcpy(&samples.data()[samples.size() - samplesLength_], samples_, samplesLength_);
            nbSamples += nbSamples_;

            av_frame_free(&frm);
        }
    }

    av_packet_unref(&pkt);
}

void FFMpegWrap::seek(const std::chrono::milliseconds &timeOffset)
{
    int64_t seekTarget = ((double) timeOffset.count() / 1000) * AV_TIME_BASE;
    seekTarget = av_rescale_q(seekTarget,
                              AV_TIME_BASE_Q,
                              m_audioContext->streams[m_streamId]->time_base);
    av_seek_frame(m_audioContext, m_streamId, seekTarget, AVSEEK_FLAG_ANY);
}

int FFMpegWrap::readBuffer(void *opaque, uint8_t *buf, int bufSize)
{
    FFMpegWrap *wrap = reinterpret_cast<FFMpegWrap*>(opaque);
    if (!wrap->m_audioLoader)
        return 0;
    int r = wrap->m_audioLoader->read(buf, bufSize, wrap->m_pos);
    wrap->m_pos += r;
    return r;
}

int64_t FFMpegWrap::seekBuffer(void *opaque, int64_t offset, int whence)
{
    FFMpegWrap *wrap = reinterpret_cast<FFMpegWrap*>(opaque);
    //    if (!wrap->m_audioLoader)
    //        return -1;
    //    if (whence == AVSEEK_SIZE)
    //        return audioStream->m_audioLoader->maxSize();
    //    int64_t newPos_ = wrap->m_audioLoader->seek(wrap->m_pos, offset, whence);
    //    if (newPos_ < 0 || newPos_ > wrap->m_audioLoader->maxSize())
    //        return -1;
    //    audioStream->m_pos = newPos_;
    return wrap->m_pos;
}

bool FFMpegWrap::decodePacket(AVPacket *packet, std::vector<AVFrame *> &frames)
{
    int ret = avcodec_send_packet(m_codecContext, packet);
    while (ret  >= 0) {
        AVFrame *frm = av_frame_alloc();
        ret = avcodec_receive_frame(m_codecContext, frm);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            av_frame_free(&frm);
            break;
        }
        frames.push_back(frm);
    }
    return true;
}

void FFMpegWrap::resampleFrame(AVFrame *frame, uint8_t *&outSamples, int &outNbSamples, int &outSamplesLength)
{
    int srcRate = frame->sample_rate;
    int outRate = frame->sample_rate;

    m_outputSamples = av_rescale_rnd(swr_get_delay(m_swrContext, srcRate) + frame->nb_samples,
                                     outRate, srcRate, AV_ROUND_UP);

    if(m_outputSamples > m_maxOutputSamples) {
        av_free(m_outputData[0]);
        av_samples_alloc(m_outputData, nullptr, m_outputChanels, m_outputSamples, AV_SAMPLE_FMT_S16, 1);
        m_maxOutputSamples = m_outputSamples;
    }

    int res = swr_convert(m_swrContext, m_outputData, m_outputSamples,
                          (const uint8_t**)frame->extended_data, frame->nb_samples);

    int outputBufferSize = av_samples_get_buffer_size(nullptr, m_outputChanels, res, AV_SAMPLE_FMT_S16, 1);

    outNbSamples = outputBufferSize / av_get_bytes_per_sample(AV_SAMPLE_FMT_S16);
    outSamplesLength = outputBufferSize;
    outSamples = m_outputData[0];
}

void FFMpegWrap::freeAll()
{
    if (m_outputData)
        av_free(m_outputData);

    if (m_swrContext)
        swr_free(&m_swrContext);

    if (m_codecContext)
        avcodec_close(m_codecContext);

    if (m_audioContext)
        avformat_close_input(&m_audioContext);
}

}
