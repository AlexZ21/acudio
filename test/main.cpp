#include <acaudio/audiostream.h>
#include <acaudio/audioplayer.h>
#include <acaudio/audioloader.h>
#include <acaudio/audiotrack.h>
#include <acaudio/ffmpegaudiostream.h>
#include <acaudio/ffmpegwrap.h>

#include <thread>

int main(int argc, char *argv[])
{
//    QApplication a(argc, argv);

    ac::AudioDevice::init();

    //ac::AudioTrack track1("1111.ogg");//("/run/media/alex/62EA986F244868FE/Music/S/indie pop & synth pop & art pop & dream pop/ЛУНА - Туман (Nekliff prod.).mp3");//("Aglory - Nyctophilia.mp3");
    ac::AudioTrack track1("Радость Моя - Беги туда, где нет никого.mp3");//("Aglory - Nyctophilia.mp3");

    ac::FFMpegAudioStream stream;
    stream.setAudioTrack(track1);
    stream.play();

//    MainWindow mw;
//    mw.show();

    std::this_thread::sleep_for(std::chrono::milliseconds(40000));

//    return a.exec();
    return 0;
}
