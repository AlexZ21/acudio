#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include <acaudio/acaudio_export.h>

namespace ac {

class ACAUDIO_EXPORT AudioPlayer
{
public:
    AudioPlayer();
};

}

#endif // AUDIOPLAYER_H
