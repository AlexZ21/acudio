#ifndef AUDIOTRACK_H
#define AUDIOTRACK_H

#include "acaudio_export.h"

#include <string>

namespace ac {

class ACAUDIO_EXPORT AudioTrack
{
public:
    AudioTrack() = default;
    AudioTrack(const std::string &source, const std::string &loader = "file");
    ~AudioTrack() = default;

    std::string source() const;
    void setSource(const std::string &source);

    std::string loader() const;
    void setLoader(const std::string &loader);

private:
    std::string m_source;
    std::string m_loader;
};

}

#endif // AUDIOTRACK_H
