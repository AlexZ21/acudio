#ifndef AUDIOLOADER_H
#define AUDIOLOADER_H

#include <acaudio/acaudio_export.h>

#include <string>
#include <map>
#include <memory>
#include <vector>

namespace ac {

class ACAUDIO_EXPORT AudioLoader
{
public:
    AudioLoader();
    virtual ~AudioLoader() = default;

    virtual void load(const std::string &url) = 0;
    virtual void stop() = 0;

    virtual int64_t read(uint8_t *data, int size, int pos) = 0;
    virtual int64_t seek(int64_t pos, int64_t offset, int whence) = 0;
};

class ACAUDIO_EXPORT FileAudioLoader : public AudioLoader
{
public:
    FileAudioLoader();
    virtual ~FileAudioLoader() = default;

    void load(const std::string &url);
    void stop();

    int64_t read(uint8_t *data, int size, int pos);
    int64_t seek(int64_t pos, int64_t offset, int whence);

private:
    std::vector<char> m_buffer;

};

class ACAUDIO_EXPORT AudioLoaderFactory
{
public:
    struct FactoryBase {
        virtual AudioLoader *create() = 0;
    };
    template <typename T>
    struct Factory : public FactoryBase {
        AudioLoader *create() { return new T(); }
    };

    template <typename T>
    void static registerLoader(const std::string &name) {
        instance().m_factories.emplace(name, std::unique_ptr<FactoryBase>(new Factory<T>()));
    }

    static AudioLoader *create(const std::string &name) {
        if (instance().m_factories.find(name) == instance().m_factories.end())
            return nullptr;
        return instance().m_factories.at(name)->create();
    }

private:
    static AudioLoaderFactory &instance() { static AudioLoaderFactory i; return i; }
    AudioLoaderFactory() { m_factories.emplace("file", std::unique_ptr<FactoryBase>(new Factory<FileAudioLoader>())); }
    ~AudioLoaderFactory() = default;

private:
    std::map<std::string, std::unique_ptr<FactoryBase>> m_factories;
};

}

#endif // AUDIOLOADER_H
