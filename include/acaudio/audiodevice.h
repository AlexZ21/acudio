#ifndef AUDIODEVICE_H
#define AUDIODEVICE_H

#include <acaudio/acaudio_export.h>

#include <AL/al.h>
#include <AL/alc.h>

#include <string>

#define alCheck(expr) do { expr; ::ac::alCheckError(__FILE__, __LINE__, #expr); } while (false)

namespace ac {

void alCheckError(const char* file, unsigned int line, const char* expression);

class ACAUDIO_EXPORT AudioDevice
{
public:
    static AudioDevice &instance();
    static bool init();

    static bool isExtensionSupported(const std::string &extension);
    static int formatFromChannelCount(unsigned int channelCount);

    static void setGlobalVolume(float volume);
    static float globalVolume();

private:
    AudioDevice();
    ~AudioDevice();

private:
    static AudioDevice *m_i;
    ALCdevice *m_audioDevice;
    ALCcontext *m_audioContext;

    float m_listenerVolume = 100.f;

};

}

#endif // AUDIODEVICE_H
