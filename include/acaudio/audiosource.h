#ifndef AUDIOSOURCE_H
#define AUDIOSOURCE_H

#include "acaudio_export.h"
#include "audiodevice.h"

namespace ac {

class ACAUDIO_EXPORT AudioSource
{
public:    
    enum Status {
        Stopped,
        Paused,
        Playing
    };

    AudioSource(const AudioSource& copy);
    virtual ~AudioSource();

    void setVolume(float volume);
    float volume() const;

    AudioSource &operator =(const AudioSource& right);

protected:
    AudioSource();

    Status status() const;

protected:
    unsigned int m_source; ///< OpenAL source identifier

};

}

#endif // AUDIOSOURCE_H
