#ifndef FFMPEGWRAP_H
#define FFMPEGWRAP_H

#include "acaudio_export.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
#include <libswresample/swresample.h>
}

#include <functional>
#include <vector>
#include <chrono>

namespace ac {

class AudioLoader;

class ACAUDIO_EXPORT FFMpegWrap
{
public:
    FFMpegWrap(AudioLoader *audioLoader, int32_t outputSampleRate, int32_t outputChanels);
    ~FFMpegWrap();

    bool init();

    int32_t outputSampleRate() const;
    int32_t outputChanels() const;

    void readSamples(std::vector<uint8_t> &samples, int &nbSamples);
    void seek(const std::chrono::milliseconds &timeOffset);

private:
    static int readBuffer(void* opaque, uint8_t* buf, int bufSize);
    static int64_t seekBuffer(void *opaque, int64_t offset, int whence);

    bool decodePacket(AVPacket *packet, std::vector<AVFrame *> &frames);
    void resampleFrame(AVFrame *frame, uint8_t *&outSamples,
                       int &outNbSamples, int &outSamplesLength);

    void freeAll();

private:
    AudioLoader *m_audioLoader;

    int32_t m_outputSampleRate;
    int32_t m_outputChanels;

    int64_t m_pos;

    std::vector<uint8_t> m_avioContextBuffer;
    AVIOContext *m_avioContext;
    AVFormatContext *m_audioContext;
    int32_t m_streamId;
    AVCodec *m_codec;
    AVCodecContext *m_codecContext;
    SwrContext *m_swrContext;
    int m_outputSamples;
    int m_maxOutputSamples;
    uint8_t **m_outputData;

};

}

#endif // FFMPEGWRAP_H
