#ifndef FFMPEGAUDIOSTREAM_H
#define FFMPEGAUDIOSTREAM_H

#include "acaudio_export.h"
#include "audiostream.h"
#include "audiotrack.h"
#include "audioloader.h"
#include "ffmpegwrap.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
#include <libswresample/swresample.h>
}

#include <memory>

namespace ac {

class ACAUDIO_EXPORT FFMpegAudioStream : public AudioStream
{
public:
    FFMpegAudioStream();
    ~FFMpegAudioStream();

    AudioTrack audioTrack() const;
    void setAudioTrack(const AudioTrack &audioTrack);

    void play();

protected:
    bool onData(Chunk &data);
    void onSeek(const std::chrono::milliseconds &timeOffset);

private:
    bool createAudioLoader();

private:
    AudioTrack m_curentAudioTrack;
    std::unique_ptr<AudioLoader> m_currentAudioLoader;
    bool m_audioTrackUpdated;

    std::unique_ptr<FFMpegWrap> m_ffmpegContext;
    std::vector<int16_t> m_outputSamplesBuffer;

};

}

#endif // FFMPEGAUDIOSTREAM_H
